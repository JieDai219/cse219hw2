package oh.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_ADD_TA_BUTTON;
import static oh.OfficeHoursPropertyType.OH_ALL_RADIO_BUTTON;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_GRADUATE_RADIO_BUTTON;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_UNDERGRADUATE_RADIO_BUTTON;
import oh.data.OfficeHoursData;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursFoolproofDesign implements FoolproofDesign {
    OfficeHoursApp app;
    
    public OfficeHoursFoolproofDesign(OfficeHoursApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        updateAddTAFoolproofDesign();
    }

    public void updateAddTAFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        // FOOLPROOF DESIGN STUFF FOR ADD TA BUTTON
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        Button addTAButton = (Button)gui.getGUINode(OH_ADD_TA_BUTTON);
        boolean isLegal = data.isLegalNewTA(name, email, "");
        if (isLegal) {
            nameTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setOnAction(addTAButton.getOnAction());
        }
        else {
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
        }
        addTAButton.setDisable(!isLegal);
        
        RadioButton rdAll = ((RadioButton) gui.getGUINode(OH_ALL_RADIO_BUTTON));
        RadioButton rdGrad = ((RadioButton) gui.getGUINode(OH_GRADUATE_RADIO_BUTTON));
        RadioButton rdUndergrad = ((RadioButton) gui.getGUINode(OH_UNDERGRADUATE_RADIO_BUTTON));
        
        if (rdAll.isSelected()) {
            nameTextField.setDisable(true);
            emailTextField.setDisable(true);
        }
        
        rdAll.setOnAction(e -> 
            {
                if (rdAll.isSelected()) {
                    nameTextField.setDisable(true);
                    emailTextField.setDisable(true);
                }
            });
        rdGrad.setOnAction(e -> 
            {
                if (rdGrad.isSelected() ) {
                    nameTextField.setDisable(false);
                    emailTextField.setDisable(false);
                }
            });
        rdUndergrad.setOnAction(e -> 
            {
                if (rdUndergrad.isSelected() ) {
                    nameTextField.setDisable(false);
                    emailTextField.setDisable(false);
                }
            });
        
    }
}